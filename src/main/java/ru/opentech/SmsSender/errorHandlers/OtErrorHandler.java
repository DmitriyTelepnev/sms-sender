package ru.opentech.SmsSender.errorHandlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;
import ru.opentech.SmsSender.props.EmailProps;
import ru.opentech.SmsSender.repositories.EmailMessageRepository;

import java.util.Arrays;

/**
 * Глобальный обработчик ошибок
 */
@Component
public class OtErrorHandler {

    private EmailMessageRepository messageRepository;

    private EmailProps emailProps;

    @Autowired
    public OtErrorHandler(EmailMessageRepository messageRepository, EmailProps emailProps) {
        this.messageRepository = messageRepository;
        this.emailProps = emailProps;
    }

    public void handleError(Throwable t) {
        messageRepository.send(
                emailProps.from,
                emailProps.source,
                emailProps.developerEmail,
                "Ошибка SmsSender",
                getMessage(t)
        );
    }

    private String getMessage(Throwable t) {
        StringBuilder sb = new StringBuilder();
        sb.append(t.getMessage()).append("\n");
        Arrays.asList(t.getStackTrace()).forEach(stackTraceElement ->
            sb
                    .append("\t")
                    .append(stackTraceElement.getClassName())
                    .append("::")
                    .append(stackTraceElement.getMethodName())
                    .append("(")
                    .append(stackTraceElement.getLineNumber())
                    .append(")\n")
        );
        return sb.toString();
    }
}
