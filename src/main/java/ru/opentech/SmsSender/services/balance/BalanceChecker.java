package ru.opentech.SmsSender.services.balance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.opentech.SmsSender.api.BalanceResponse;
import ru.opentech.SmsSender.api.Request;
import ru.opentech.SmsSender.errorHandlers.OtErrorHandler;
import ru.opentech.SmsSender.props.ApiProps;
import ru.opentech.SmsSender.props.EmailProps;
import ru.opentech.SmsSender.repositories.BalanceRepository;
import ru.opentech.SmsSender.repositories.EmailMessageRepository;

import java.security.NoSuchAlgorithmException;

/**
 * Сервис проверки баланса
 */
@Service
public class BalanceChecker {

    private static final Logger log = LoggerFactory.getLogger( BalanceChecker.class );

    private ApiProps props;

    private EmailProps emailProps;

    private BalanceRepository balanceRepository;

    private EmailMessageRepository messageRepository;

    private OtErrorHandler errorHandler;

    @Autowired
    BalanceChecker(
            BalanceRepository balanceRepository,
            EmailMessageRepository messageRepository,
            ApiProps props,
            EmailProps emailProps,
            OtErrorHandler errorHandler
    ) {
        this.balanceRepository = balanceRepository;
        this.messageRepository = messageRepository;
        this.props = props;
        this.emailProps = emailProps;
        this.errorHandler = errorHandler;
    }

    @Scheduled(cron = "${application.cron.checkBalance}")
    public void checkBalance() {
        try {
            BalanceResponse balanceResponse = readBalance();
            Double yesterdaySpentCash = balanceRepository.readYesterdaySpentCash();

            if(balanceResponse.balance < yesterdaySpentCash) {
                messageRepository.send(
                        emailProps.from,
                        emailProps.source,
                        emailProps.reportEmail,
                        emailProps.subject,
                        getMessage(balanceResponse.balance, yesterdaySpentCash)
                );
            } else {
                log.info("Текущий баланс: {}", balanceResponse.balance);
            }
        } catch(Throwable t) {
            errorHandler.handleError(t);
            log.error("Произошла ошибка при проверке баланса", t);
        }
    }

    private BalanceResponse readBalance() throws NoSuchAlgorithmException {
        return new Request(Request.Type.GET_BALANCE, props)
                .getResponseForObject(BalanceResponse.class);
    }

    private String getMessage(double currentBalance, double yesterdaySpentCash) {
        return String.format(
                "Текущих средств не хватит на текщий день. Необходимо пополнить баланс\n" +
                "Текущий баланс - %f\n" +
                "За прошлый день потрачено - %f",
                currentBalance,
                yesterdaySpentCash
        );
    }

}
