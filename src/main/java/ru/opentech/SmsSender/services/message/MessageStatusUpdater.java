package ru.opentech.SmsSender.services.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.opentech.SmsSender.api.GetStatusResponse;
import ru.opentech.SmsSender.api.Request;
import ru.opentech.SmsSender.api.Response;
import ru.opentech.SmsSender.errorHandlers.OtErrorHandler;
import ru.opentech.SmsSender.props.ApiProps;
import ru.opentech.SmsSender.model.Message;
import ru.opentech.SmsSender.repositories.MessageRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MessageStatusUpdater {

    private static final Logger log = LoggerFactory.getLogger( MessageStatusUpdater.class );

    private MessageRepository messageRepository;

    @Value("${application.smsChunk}")
    private int smsChunk;

    private ApiProps props;

    private OtErrorHandler errorHandler;

    @Autowired
    MessageStatusUpdater(MessageRepository messageRepository, ApiProps props, OtErrorHandler errorHandler) {
        this.props = props;
        this.messageRepository = messageRepository;
        this.errorHandler = errorHandler;
    }

    @Scheduled(fixedRateString = "${application.sleepMs}")
    public void updateStatus() {
        try {
            Message.Status[] messageStatuses = new Message.Status[] {
                    Message.Status.SENT,
                    Message.Status.ENQUEUED,
                    Message.Status.ACCEPTED,
                    Message.Status.SCHEDULED
            };
            log.info("Запрашиваю сообщения в статусах {}", Arrays.toString(messageStatuses));
            List<Message> messageList = messageRepository
                    .findMessageByStatusIn(
                            messageStatuses,
                            new PageRequest(0, smsChunk)
                    );

            if(!messageList.isEmpty()) {
                List<Message> changedMessages = fillStatuses(messageList);
                messageRepository.save(changedMessages);
                log.info("Обновлено сообщений {}", changedMessages.size());
            } else {
                log.info("Нет сообщений для обновления состояний");
            }
        } catch(Throwable t) {
            errorHandler.handleError(t);
            log.error("Произошла ошибка при запросе состояний СМС", t);
        }
    }

    /**
     * Получение статуса сообщения из стороннего сервиса рассылки
     * @param messageList
     */
    private List<Message> fillStatuses(List<Message> messageList) throws Throwable {
        log.info("Запрашиваю состояния сообщений у сервиса");
        GetStatusResponse response = new Request(Request.Type.GET_MESSAGE_STATUS, props)
                .setParams(getParams(messageList))
                .getResponseForObject(GetStatusResponse.class);
        if(response.status.equals(Response.Status.ERROR)) {
            log.info("Запрос состояний сообщений вернул ошибку: ", response.message);
        }

        return fillMessagesByResponse(messageList, response);
    }

    /**
     * Сбор парасметров для Request
     * @param messageList
     * @return params
     */
    private Map<String, String> getParams(List<Message> messageList) {
        List<Integer> messageIds = new ArrayList<>();
        messageList.forEach(message -> messageIds.add(message.serviceMessageId));

        String concatenatedMessageIds = StringUtils.collectionToDelimitedString(messageIds, ",");
        Map<String, String> params = new HashMap<>();
        params.put(Request.Param.MESSAGE_IDS.getName(), concatenatedMessageIds);
        return params;
    }

    /**
     * Заполнение сообщения данными из Response
     * @param messageList
     * @param response
     */
    private List<Message> fillMessagesByResponse(List<Message> messageList, GetStatusResponse response) {
        return messageList.stream()
                .filter(message -> {
                    if(response.status.equals(Response.Status.ERROR)) {
                        message.errorMessage = response.message;
                        return message.changeStatus(Message.Status.FAILED);
                    } else {
                        return message.changeStatus(
                            Message.Status.of(response.messageStatuses.get(message.serviceMessageId))
                        );
                    }
                })
                .collect(Collectors.toList());
    }

}
