package ru.opentech.SmsSender.services.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.opentech.SmsSender.api.Request;
import ru.opentech.SmsSender.api.SentMessagesResponse;
import ru.opentech.SmsSender.errorHandlers.OtErrorHandler;
import ru.opentech.SmsSender.props.ApiProps;
import ru.opentech.SmsSender.api.Response;
import ru.opentech.SmsSender.model.Message;
import ru.opentech.SmsSender.repositories.MessageRepository;

import java.util.*;

/**
 * Сервис рассылки сообщений
 */
@Service
public class MessageSender {

    private static final Logger log = LoggerFactory.getLogger( MessageSender.class );

    private MessageRepository messageRepository;

    @Value("${application.smsChunk}")
    private int smsChunk;

    /**
     * Телефон, на который нужно слать сообщения, остальные не слать
     * Нужен для переключения приложения в отладочный режим
     */
    @Value("${phone:#{null}}")
    private String theOnlyProcessingPhoneNumber;

    @Value("${mainsms.sender}")
    private String sender;

    private ApiProps props;

    private OtErrorHandler errorHandler;

    @Autowired
    MessageSender(
            MessageRepository messageRepository,
            ApiProps props,
            OtErrorHandler errorHandler
    ) {
        this.messageRepository = messageRepository;
        this.props = props;
        this.errorHandler = errorHandler;
    }

    @Scheduled(fixedRateString = "${application.sleepMs}")
    public void checkAndSend() {
        try {
            checkAndFixSendingMessages();
            List<Message> messageList = getMessageList();

            if(!messageList.isEmpty()) {
                for(Message message : messageList) {
                    send(message);
                }
                messageRepository.save(messageList);
                log.info("Отправлено {} сообщений", messageList.size());
            } else {
                log.info("Нет сообщений для отправки");
            }
        } catch(Throwable t) {
            errorHandler.handleError(t);
            log.error("Произошла ошибка при отправке СМС", t);
        }
    }

    private List<Message> getMessageList() {
        List<Message> messageList;
        if(theOnlyProcessingPhoneNumber != null) {
            messageList = getNotSentMessageListByPhone();
        } else {
            messageList = getNotSentMessageList();
        }
        return messageList;
    }

    private List<Message> getNotSentMessageListByPhone() {
        return messageRepository
                .findMessageByPhoneAndStatusIn(
                        theOnlyProcessingPhoneNumber,
                        new Message.Status[]{Message.Status.NOT_SEND},
                        new PageRequest(0, smsChunk)
                );
    }

    private List<Message> getNotSentMessageList() {
        return messageRepository
                .findMessageByStatusIn(
                        new Message.Status[]{Message.Status.NOT_SEND},
                        new PageRequest(0, smsChunk)
                );
    }

    /**
     * Отправка сообщения
     * @param message
     */
    private void send(Message message) throws Throwable {

        message.status = Message.Status.SENDING;
        messageRepository.save(message);

        log.info("Отправляю сообщение id#{} на номер {}", message.id, message.phone);

        SentMessagesResponse response = new Request(Request.Type.SEND_MESSAGE, props)
                .setParams(createParamsByMessage(message))
                .getResponseForObject(SentMessagesResponse.class);

        log.info("Сообщение id#{} отпралено", message.id);
        fillMessageByResponse(message, response);

    }

    /**
     * Сбор параметров для Request
     * @param message
     * @return params
     */
    private Map<String, String> createParamsByMessage(Message message) {
        Map<String, String> params = new HashMap<>();
        params.put(Request.Param.MESSAGE.getName(), message.message);
        params.put(Request.Param.RECIPIENTS.getName(), message.phone);
        params.put(Request.Param.SENDER.getName(), sender);

        return params;
    }

    /**
     * Заполнение сообщения данными из Response
     * @param message
     * @param response
     */
    private void fillMessageByResponse(Message message, SentMessagesResponse response) {
        if(response.status.equals(Response.Status.SUCCESS.getStatus())) {
            Integer[] messageIds = response.messageIds;
            message.serviceMessageId = messageIds[0];
            message.status = Message.Status.SENT;
        } else {
            message.status = Message.Status.FAILED;
            message.errorMessage = response.message;
        }
        message.timeSent = new Date();
    }

    /**
     * Если мы не смогли отправить смс, но уже перевели ее в состояние "Отправляется" - проставить статус "Ошибка"
     */
    private void checkAndFixSendingMessages() {
        List<Message> messageList = messageRepository
                .findMessageByStatusIn(
                        new Message.Status[] {Message.Status.SENDING},
                        new PageRequest(0, smsChunk)
                );
        if(!messageList.isEmpty()) {
            messageList.forEach(message -> message.status = Message.Status.FAILED);
            messageRepository.save(messageList);
            log.info("Переведено сообщений {} из статуса \"Отправляется\" в статус \"Ошибка\"", messageList.size());
        }
    }

}
