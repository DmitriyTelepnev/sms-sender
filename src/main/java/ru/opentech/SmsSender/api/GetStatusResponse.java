package ru.opentech.SmsSender.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Класс ответа от API при запросе состояния сообщения
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetStatusResponse extends Response {

    @JsonProperty("messages")
    public Map<Integer, String> messageStatuses;

}
