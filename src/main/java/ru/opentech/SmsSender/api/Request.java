package ru.opentech.SmsSender.api;

import org.springframework.web.client.RestTemplate;
import ru.opentech.SmsSender.props.ApiProps;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс отправки запроса к API
 */
public class Request {

    private Type requestType;

    private RestTemplate template;

    private Map<String, String> params = new HashMap<>();

    private Signature signature;

    private ApiProps apiProps;

    public Request(Type type, ApiProps props) {
        template = new RestTemplate();
        params.put(Param.PROJECT.getName(), props.project);
        if(props.testMode) {
            params.put(Param.TEST_MODE.getName(), "1");
        }
        signature = new Signature(props);
        apiProps = props;
        requestType = type;
    }

    public Request setParams(Map<String, String> params) {
        this.params.putAll(params);
        return this;
    }

    public <T> T getResponseForObject(Class<T> type) throws NoSuchAlgorithmException {
        params.put(Param.SIGN.getName(), signature.get(params));

        return template.getForObject(getUrl(), type, params);
    }

    private String getUrl() {
        return apiProps.urlBase + requestType.getUrl();
    }

    public enum Type {
        GET_BALANCE("balance?project={project}&sign={sign}"),
        SEND_MESSAGE("send?project={project}&recipients={recipients}&message={message}&sender={sender}&sign={sign}"),
        GET_MESSAGE_STATUS("status?project={project}&messages_id={messages_id}&sign={sign}");

        private String url;

        Type(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

    public enum Param {
        SENDER("sender"),
        PROJECT("project"),
        RECIPIENTS("recipients"),
        MESSAGE("message"),
        MESSAGE_IDS("messages_id"),
        SIGN("sign"),
        TEST_MODE("test");

        private String name;

        Param(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
