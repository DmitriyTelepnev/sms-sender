package ru.opentech.SmsSender.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Класс ответа от API при отправке сообщения
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SentMessagesResponse extends Response {

    @JsonProperty("messages_id")
    public Integer[] messageIds;

    public double balance;

    @JsonProperty("cost")
    public double price;

}
