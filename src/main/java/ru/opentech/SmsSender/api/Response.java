package ru.opentech.SmsSender.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * базовый класс ответа от API
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private static final Logger log = LoggerFactory.getLogger( Response.class );

    public String status;

    public String message;

    public void check() {
        if(status.equals(Response.Status.ERROR.getStatus())) {
            log.error(message);
        }
    }

    public String toString() {
        return "{status: " + status + ", message: " + message + "}";
    }

    public enum Status {
        ERROR("error"),
        SUCCESS("success");

        private String status;

        Status(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

}
