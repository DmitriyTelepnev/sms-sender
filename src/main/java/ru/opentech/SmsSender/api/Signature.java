package ru.opentech.SmsSender.api;

import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;
import ru.opentech.SmsSender.props.ApiProps;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Класс формирования подписи
 * https://mainsms.ru/home/api#sign
 *
 */
@Component
class Signature {

    private ApiProps props;

    Signature(ApiProps props) {
        this.props = props;
    }

    String get(Map<String, String> params) throws NoSuchAlgorithmException {
        return hash(concatenateParams(params));
    }

    private String concatenateParams(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        List<String> sortedKeys=new ArrayList<>(params.keySet());
        Collections.sort(sortedKeys);
        sortedKeys.forEach(key -> sb.append(params.get(key)).append(';'));
        return sb.append(props.apiKey).toString();
    }

    private String hash(String concatenatedParams) throws NoSuchAlgorithmException {
        byte[] buf = concatenatedParams.getBytes( Charset.forName( "UTF-8" ) );

        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        MessageDigest md5 = MessageDigest.getInstance("MD5");

        buf = sha1.digest( buf );
        buf = md5.digest( Hex.encodeHexString( buf ).getBytes() );


        return Hex.encodeHexString( buf );
    }

}
