package ru.opentech.SmsSender.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Сообщение
 */
@Entity
@Table(name = "messages", schema = "sms")
public class Message implements Serializable {

    @Id
    public Long id;
    public String message;
    public String phone;
    public Status status;
    @Column(name = "id_sms_service")
    public Integer serviceMessageId;
    @Column(name = "tm_sent")
    public Date timeSent;
    @Column(name = "tm_delivered")
    public Date timeDelivered;
    @Column(name = "error_message")
    public String errorMessage;

    public boolean changeStatus(Message.Status newStatus) {
        if(status != newStatus) {
            timeDelivered = new Date();
            status = newStatus;
            return true;
        }
        return false;
    }

    public String toString() {
        return "{" + phone + ", '" + message + "', " + status  + ", " + serviceMessageId + "}";
    }

    /**
     * Наши состояния сообщений
     */
    public enum Status {
        NOT_SEND,
        SENDING,
        SENT,
        ENQUEUED,
        ACCEPTED,
        DELIVERED,
        FAILED,
        NON_DELIVERED,
        SCHEDULED;

        public static Status of(String name) {
            return Enum.valueOf( Status.class, name.replace( '-', '_' ).toUpperCase() );
        }
    }

}
