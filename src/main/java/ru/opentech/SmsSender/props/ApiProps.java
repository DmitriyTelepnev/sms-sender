package ru.opentech.SmsSender.props;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mainsms")
@Setter
public class ApiProps {
    public boolean testMode;
    public String project;
    public String urlBase;
    public String apiKey;
}
