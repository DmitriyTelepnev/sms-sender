package ru.opentech.SmsSender.props;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "application.email")
@Setter
public class EmailProps {
    public String from;
    public String developerEmail;
    public String reportEmail;
    public String source;
    public String subject;
}
