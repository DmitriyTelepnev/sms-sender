package ru.opentech.SmsSender.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Репозиторий Email-сообщений
 */
@Component
public class EmailMessageRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    EmailMessageRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void send(
            String from,
            String source,
            String recipient,
            String subject,
            String body
    ) {
            jdbcTemplate.queryForObject(
                    "SELECT mailsender.send_simple(?,?,?,?,?)",
                    Integer.class,
                    from, source,
                    recipient,
                    subject,
                    body
            );
    }

}
