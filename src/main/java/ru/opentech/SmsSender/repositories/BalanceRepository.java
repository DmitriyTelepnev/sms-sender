package ru.opentech.SmsSender.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Репозиторий для запроса баланса
 */
@Component
public class BalanceRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    BalanceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Чтение израсходованных средств за прошлый день
     * @return yesterdaySpentCash
     */
    public Double readYesterdaySpentCash() {
        return jdbcTemplate.queryForObject(
                "SELECT COALESCE(SUM(price), 0.0) balance\n" +
                "FROM sms.messages\n" +
                "WHERE tm_sent BETWEEN CURRENT_DATE - interval '1 day' AND CURRENT_DATE ",
                Double.class
        );
    }

}
