package ru.opentech.SmsSender.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.opentech.SmsSender.model.Message;

import java.util.List;

/**
 * Репозиторий СМС-сообщений
 */
@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findMessageByStatusIn(Message.Status[] statuses, Pageable pageable);
    List<Message> findMessageByPhoneAndStatusIn(String phone, Message.Status[] statuses, Pageable pageable);

}
